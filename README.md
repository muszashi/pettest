# Pet store smoke/integration test
The pet store smoke/integration test contains a basic framework and basic smoke test for verifying the RESTful APIs of
`/pet` endpoint on the `https://petstore.swagger.io/#/` site

# Project/technology description
##Base project setup:
* Java 8
* Maven
* Junit 5
* Rest assured

##Plugins:
* Maven surefire plugin - for unit test
* Maven failsafe plugin - for the smoke/integration test
* Junit reporter plugin - for HTML based reports based on the Junit 5 test results

##Version control:
The project is using 
* Git for version control
* BitBucket as repository -> `https://bitbucket.org/muszashi/pettest`

#Framework
* The framework based on Rest Assured
* It has some flexibility about the endpoints, properties
* The framework itself should be tested as well as any other code. Sample unit test is created: `PropertyReaderTest`
* It is not finalized but at least showing the basic idea and the way of how can it be extended

#Smoke test
* The `PetIT` test contains the smoke test of the Pet store and testing the basic functionalities
* It contains one happy flow test to test the followings:
    * Create pet
    * Get pet by id
    * Update pet 
    * Get pet by status
    * Delete pet
 * As the test runs against "production" like shared environment, it is really important to avoid pollution and 
 collisions. 
 * Important that the test should be maintenable, repeatable
 * To achieving these goals, it is using randomly generated ids and also runs clean up at the end
 * As it is an integration type smoke test, important to use the product functionality to verify the results of other 
 functionalities
    * So when it creates a pet verifies the response
    * But in the second step ir get the same pet by ide and verifies if gives back the same data as in the creation
    * When updates the status of the pet then in the next step get all the pets by the updated status and verifies if t
    he updated pet is in the list or not
    * As part of the tear down, the pet is deleted (so the deletion is implicitly tested) and verifies if the deletion 
    was successful or not

#Test executuon
The test can be executed by the following maven command: 
* `mvn clean verify -Pintegration-test`  

#Test report
The test report is very basic html report based on the Junit execution. The report is place into the 
`target/junit-reporter' folder
It could be improved several ways, with custom reporter, adding BDD layer (Cucumber, JBehave, etc) to it combining 
the gherkin based approach with the Rest assured technology, etc.

#CI/CD 
As per basic setting the test running automatically on pipeline on master branch:
* `https://bitbucket.org/muszashi/pettest/addon/pipelines/home`
  





