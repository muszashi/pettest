package org.epam.petstore.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

import static java.lang.System.lineSeparator;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Property reader test")
class PropertyReaderTest {

    @Test
    @DisplayName("Verify reading property from file")
    void verifyGetProperty() throws IOException {

        PropertyReader propertyReader = new PropertyReader(createTempFile());
        assertEquals(Optional.of("property test"), propertyReader.getProperty("test.property"));
    }

    private File createTempFile() throws IOException {
        File file = File.createTempFile("tempfile", ".tmp");
        file.deleteOnExit();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write("test.property=property test" + lineSeparator() + "test.property.2=property test 2");
        bw.close();
        return file;
    }
}