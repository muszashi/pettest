package org.epam.petstore.integration;

import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.epam.petstore.domain.Category;
import org.epam.petstore.domain.Pet;
import org.epam.petstore.domain.PetTag;
import org.epam.petstore.domain.Status;
import org.epam.petstore.util.PetStoreTestException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import static org.epam.petstore.domain.Status.available;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("CRUD test for pet store API")
public class PetIT extends TestBase {


    private static final Category CAT = new Category(1, "Cat");
    private static final PetTag COLOR = new PetTag(1, "Gray");
    private static final int MAX_NUM_BITS = 20;
    private static final String NAME_JSON_FIELD = "name";

    @Test
    @Tag("create")
    @Tag("getById")
    @Tag("HappyFlow")
    @DisplayName("Test basic flow of create, get, update and delete pet in pet store")
    public void basicPetStoreFlowIT() {


        Status updateStatus = available;
        /* Creating random id to avoid collision or false positive test because of previous test runs or other users' interactions
         * There is still chance to have collision but the risk is minimal.
         */
        BigInteger petId = new BigInteger(MAX_NUM_BITS, new SecureRandom());

        Pet pet = Pet.builder()
                     .id(petId)
                     .name("Tom")
                     .status(Status.pending)
                     .category(CAT)
                     .tags(new PetTag[]{COLOR})
                     .build();

        verifyPetCreation(pet);

        // petIdForMaintenance only set after successful pet creation so

        petIdForMaintenance = pet.getId();

        verifyGetPet(pet);
        verifyUpdateStatus(pet.getId(), updateStatus);
        verifyGetByStatus(updateStatus, pet);
    }

    private void verifyPetCreation(Pet pet) {
        // Create pet calling /pet endpoint and validate status code and basic body content
        ValidatableResponse response = getApi().create(pet)
                                               .log()
                                               .all(true)
                                               .statusCode(HttpStatus.SC_OK)
                                               .body(NAME_JSON_FIELD, is(pet.getName()));

        // Extract body and deserialize as Pet.class and compare with the requested Pet
        Pet responsePet = response.extract()
                                  .body()
                                  .as(Pet.class);
        assertEquals(pet, responsePet);
    }

    private void verifyGetPet(Pet pet) {
        /* There are 2 reasons to get pet by id:
         * 1) Testing the getById api
         * 2) Using the application itself to verify the result of the create pet
         *
         * Verify petIdForMaintenance, response code and validate the pet
         */
        ValidatableResponse response = getApi().getById(pet.getId())
                                               .log()
                                               .all(true)
                                               .statusCode(HttpStatus.SC_OK)
                                               .body(NAME_JSON_FIELD, is(pet.getName()));
        // Validate against requested pet
        Pet responsePet = response.extract()
                                  .body()
                                  .as(Pet.class);
        assertEquals(pet, responsePet);
    }

    private void verifyUpdateStatus(BigInteger petId, Status status) {
        // Update pet status to available and check the response code
        Map<String, String> formData = new HashMap<>();
        formData.put("status", status.toString());

        getApi().update(petId, formData)
                .log()
                .all(true)
                .statusCode(HttpStatus.SC_OK);
    }

    private void verifyGetByStatus(Status status, Pet pet) {
        //Get all pets by status and verify if my pet is part of it

        Pet petFromList = getApi().getByStatus(status)
                                  .statusCode(HttpStatus.SC_OK)
                                  .extract()
                                  .body()
                                  .jsonPath()
                                  .getList(".", Pet.class)
                                  .stream()
                                  .filter(filterPet -> filterPet.getId().equals(pet.getId()))
                                  .findFirst()
                                  .orElseThrow(() -> new PetStoreTestException("Pet not found in the list"));


        assertAll("Pet should have the updated status and all the information from the original request",
                  () -> assertEquals(pet.getId(), petFromList.getId()),
                  () -> assertEquals(pet.getCategory(), petFromList.getCategory()),
                  () -> assertEquals(available, petFromList.getStatus()),
                  () -> assertArrayEquals(pet.getTags(), petFromList.getTags()),
                  () -> assertEquals(pet.getName(), petFromList.getName()));
    }
}
