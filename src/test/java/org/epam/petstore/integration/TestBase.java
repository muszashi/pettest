package org.epam.petstore.integration;

import org.apache.http.HttpStatus;
import org.epam.petstore.api.PetStoreApi;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Optional;

public abstract class TestBase {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);

    private static PetStoreApi api;

    static BigInteger petIdForMaintenance;

    @BeforeAll
    public static void setUp() {
        LOGGER.info("Setting up API");
        petIdForMaintenance = null;
        api = new PetStoreApi();
    }

    @BeforeEach
    void init(TestInfo testInfo) {
        LOGGER.info("\"{}\" test started", testInfo.getDisplayName());
    }

    @AfterEach
    void tearDown(TestInfo testInfo) {
        LOGGER.info("\"{}\" test finished", testInfo.getDisplayName());
    }

    @AfterAll
    public static void cleanUp() {
        if (Optional.ofNullable(petIdForMaintenance).isPresent()) {
            LOGGER.info("Cleaning up pet {}", petIdForMaintenance);
            api.delete(petIdForMaintenance)
               .log()
               .all(true)
               .statusCode(HttpStatus.SC_OK);
        }
    }

    PetStoreApi getApi() {
        return api;
    }
}
