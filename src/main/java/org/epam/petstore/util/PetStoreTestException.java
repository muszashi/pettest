package org.epam.petstore.util;

public class PetStoreTestException extends RuntimeException {

    public PetStoreTestException() {
        super();
    }

    public PetStoreTestException(Throwable cause) {
        super(cause);
    }

    public PetStoreTestException(String message) {
        super(message);
    }

    public PetStoreTestException(String message, Throwable cause) {
        super(message, cause);
    }


}
