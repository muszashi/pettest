package org.epam.petstore.util;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Optional;
import java.util.Properties;

public class PropertyReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyReader.class);

    private Properties properties = new Properties();

    public PropertyReader(@NonNull File propertiesFile) {
        try {
            properties.load(new FileInputStream(propertiesFile));
        } catch (IOException exception) {
            LOGGER.debug("Cannot find or load property file: {}, falling back on system properties", propertiesFile.getName());
        }
    }

    public PropertyReader(@NonNull final String fileName) {
        try {
            final InputStream propertyStream = Optional.ofNullable(this.getClass()
                                                                       .getClassLoader()
                                                                       .getResourceAsStream(fileName))
                                                       .orElseThrow(() -> new IOException("Stream is null"));

            properties.load(propertyStream);

        } catch (IOException exception) {
            LOGGER.debug("Cannot find or load property file: {}, falling back on system properties", fileName);
        }
    }

    public Optional<String> getProperty(@NonNull final String propertyName) {
        return Optional.ofNullable(properties.getProperty(propertyName, System.getProperty(propertyName)));
    }
}
