package org.epam.petstore.api;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import lombok.NonNull;
import org.epam.petstore.domain.Pet;
import org.epam.petstore.domain.Status;
import org.epam.petstore.util.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Map;

public class PetStoreApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(PetStoreApi.class);
    private static final String CONNECTION_PROPERTIES_FILE = "connection.properties";
    private static final String PORT_PROPERTY_NAME = "petstore.port";
    private static final String URI_PROPERTY_NAME = "petstore.uri";
    private static final String BASE_PATH_PROPERTY_NAME = "petstore.basePath";
    private static final String QUERY_PARAMETER_STATUS = "status";
    private static final String FIND_BY_STATUS_URI = "/findByStatus";
    private static final String APPLICATION_JSON_CONTENT_TYPE = "application/json";

    private final RequestSpecification specification;

    public PetStoreApi() {

        final PropertyReader propertyReader = new PropertyReader(CONNECTION_PROPERTIES_FILE);

        final int port = propertyReader.getProperty(PORT_PROPERTY_NAME)
                                       .map(Integer::valueOf)
                                       .orElse(RestAssured.DEFAULT_PORT);
        final String uri = propertyReader.getProperty(URI_PROPERTY_NAME)
                                         .orElse(RestAssured.DEFAULT_URI);
        final String basePath = propertyReader.getProperty(BASE_PATH_PROPERTY_NAME)
                                              .orElse(RestAssured.DEFAULT_PATH);

        specification = RestAssured.given()
                                   .log()
                                   .all(true)
                                   .spec(new RequestSpecBuilder().setUrlEncodingEnabled(false)
                                                                 .setPort(port)
                                                                 .setBaseUri(uri)
                                                                 .setBasePath(basePath)
                                                                 .build());

        LOGGER.info("Setting up request specification @ {}:{}{}", uri, port, basePath);
    }

    public ValidatableResponse create(@NonNull Pet pet) {

        LOGGER.info("Creating pet: {}", pet);
        return defaultSpecification().when()
                                     .accept(APPLICATION_JSON_CONTENT_TYPE)
                                     .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                     .body(pet)
                                     .post()
                                     .then();
    }

    public ValidatableResponse update(@NonNull Pet pet) {
        LOGGER.info("Updating pet: {}", pet);
        return defaultSpecification().when()
                                     .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                     .body(pet)
                                     .put()
                                     .then();
    }

    public ValidatableResponse update(@NonNull BigInteger petId, Map<String, String> formData) {
        LOGGER.info("Updating pet: {} with info", petId, formData);
        RequestSpecification updateSpecification = defaultSpecification().when()
                                                                         .contentType(ContentType.URLENC
                                                                                              .withCharset("UTF-8"));
        formData.forEach(updateSpecification::formParam);
        return updateSpecification.pathParam("petId", petId)
                                  .post("/{petId}")
                                  .then();
    }

    public ValidatableResponse getByStatus(@NonNull Status status) {
        LOGGER.info("Getting pet(s) by status: {}", status);
        return defaultSpecification().when()
                                     .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                     .queryParam(QUERY_PARAMETER_STATUS, status)
                                     .get(FIND_BY_STATUS_URI)
                                     .then();
    }

    public ValidatableResponse getById(@NonNull BigInteger petId) {
        LOGGER.info("Getting pet by petId: {}", petId);
        return defaultSpecification().when()
                                     .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                     .pathParam("petId", petId)
                                     .get("/{petId}")
                                     .then();
    }

    public ValidatableResponse delete(@NonNull BigInteger petId) {
        LOGGER.info("Deleting pet by petId: {}", petId);
        return defaultSpecification().when()
                                     .contentType(APPLICATION_JSON_CONTENT_TYPE)
                                     .pathParam("petId", petId)
                                     .delete("/{petId}", petId)
                                     .then();
    }

    private RequestSpecification defaultSpecification() {
        return RestAssured.given()
                          .spec(specification);
    }
}
