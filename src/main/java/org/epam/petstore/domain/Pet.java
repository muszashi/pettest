package org.epam.petstore.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pet {
    private BigInteger id;
    private Category category;
    private String name;
    private String[] photoUrls;
    private PetTag[] tags;
    private Status status;
}
