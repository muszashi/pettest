package org.epam.petstore.domain;

public enum Status {
    available, pending, sold
}
